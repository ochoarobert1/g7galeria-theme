<?php get_header(); ?>
<?php the_post(); ?>
<section class="page-container col-lg-9 col-md-9 col-sm-9 col-xs-9" role="article" itemscope itemtype="http://schema.org/BlogPosting">
    <article id="post-<?php the_ID(); ?>" class="page-content <?php echo join(' ', get_post_class()); ?>" >
        <div class="page-article single-main-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" itemprop="articleBody">
            <?php $defaultargs = array('class' => 'img-responsive'); ?>
            <article id="post-<?php the_ID(); ?>" class="the-single single-artists-container col-lg-12 col-md-12 col-sm-12 col-xs-12 <?php echo join(' ', get_post_class()); ?>" itemscope itemtype="http://schema.org/Article">
                <header>
                    <h1 itemprop="name"><?php the_title(); ?></h1>
                    <?php $artist_id = get_post_meta(get_the_ID(), 'rw_expo_artist', true); ?>
                    <?php $post_artist = get_post($artist_id); ?>
                    <p>Por: <?php echo $post_artist->post_title; ?></p>
                </header>

                <div class="post-content" itemprop="articleBody">
                    <?php the_content() ?>
                    <?php $files = rwmb_meta( 'rw_cat_file' ); ?>
                    <?php if ( !empty( $files ) ) { ?>
                    <?php foreach ( $files as $file ) { echo "<a href='{$file['url']}' title='{$file['title']}' target='_blank'><i class='fa fa-download fa-5x' aria-hidden='true'></i></a>"; } } ?>
                    <?php wp_link_pages( array(
    'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'g7galeria' ) . '</span>',
    'after'       => '</div>',
    'link_before' => '<span>',
    'link_after'  => '</span>', ) ); ?>
                </div><!-- .post-content -->
                <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                <meta itemprop="url" content="<?php the_permalink() ?>">
                <hr>

            </article> <?php // end article ?>


        </div>
    </article>
</section>
<?php get_footer(); ?>
