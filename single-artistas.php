<?php get_header(); ?>
<?php the_post(); ?>
<section class="page-container col-lg-9 col-md-9 col-sm-9 col-xs-9" role="article" itemscope itemtype="http://schema.org/BlogPosting">
    <article id="post-<?php the_ID(); ?>" class="page-content <?php echo join(' ', get_post_class()); ?>" >
        <div class="page-article single-main-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" itemprop="articleBody">
            <?php $defaultargs = array('class' => 'img-responsive'); ?>
            <?php /* GET THE POST FORMAT */ ?>

            <?php /* POST FORMAT - DEFAULT */ ?>

            <article id="post-<?php the_ID(); ?>" class="the-single single-artists-container col-lg-9 col-md-9 col-sm-9 col-xs-9 no-paddingl <?php echo join(' ', get_post_class()); ?>" itemscope itemtype="http://schema.org/Article">

                <header>
                    <h1 itemprop="name"><?php the_title(); ?></h1>
                </header>
                <div class="post-content" itemprop="articleBody">
                    <?php the_content() ?>
                    <?php wp_link_pages( array(
    'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'g7galeria' ) . '</span>',
    'after'       => '</div>',
    'link_before' => '<span>',
    'link_after'  => '</span>', ) ); ?>
                </div><!-- .post-content -->
                <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                <meta itemprop="url" content="<?php the_permalink() ?>">
                <h3 class="single-artists-expo-title">Exposiciones</h3>
                <hr>
                <div class="archive-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <?php $defaultatts = array('class' => 'img-responsive'); ?>
                    <?php $artist_id = get_the_ID(); ?>
                    <?php $args = array('post_type' => 'exposiciones', 'posts_per_page' => -1, 'meta_key' => 'rw_expo_artist', 'meta_query' => array( array( 'key' => 'rw_expo_artist', 'value' => $artist_id ))); ?>
                    <?php query_posts($args); ?>
                    <?php while (have_posts()) : the_post(); ?>
                    <article id="post-<?php the_ID(); ?>" class="archive-item col-lg-4 col-md-4 col-sm-4 col-xs-4 <?php echo join(' ', get_post_class()); ?>" role="article">
                        <picture class="archive-item-picture-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                            <?php if ( has_post_thumbnail()) : ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                <?php the_post_thumbnail('artist_img', $defaultatts); ?>
                            </a>
                            <?php else : ?>
                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/no-img.jpg" alt="No img" class="img-responsive" />
                            </a>
                            <?php endif; ?>
                            <div class="archive-item-title-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><h2 itemprop="title" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2></a>
                            </div>
                        </picture>
                    </article>
                    <?php endwhile; ?>
                </div>
            </article> <?php // end article ?>
            <aside class="single-artists-aside col-lg-3 col-md-3 col-sm-3 col-xs-3 no-paddingr" role="complementary">
                <div id="sticker-artists">
                    <?php if ( has_post_thumbnail()) : ?>
                    <picture>
                        <?php the_post_thumbnail('single_img', $defaultargs); ?>
                    </picture>
                    <?php endif; ?>
                    <h3>+ Info:</h3>
                    <?php ?>
                    <ul>
                        <li>nombre</li>
                        <li>nombre</li>
                        <li>nombre</li>
                        <li>nombre</li>
                        <li>nombre</li>
                        <li>nombre</li>
                    </ul>
                </div>
            </aside>

        </div>
    </article>
</section>
<?php get_footer(); ?>
