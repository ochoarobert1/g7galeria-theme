<?php get_header(); ?>
<?php the_post(); ?>
<section class="page-container col-lg-9 col-md-9 col-sm-9 col-xs-9" role="article" itemscope itemtype="http://schema.org/BlogPosting">
    <article id="post-<?php the_ID(); ?>" class="page-content <?php echo join(' ', get_post_class()); ?>" >
        <div class="page-article single-main-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" itemprop="articleBody">
            <?php $defaultargs = array('class' => 'img-responsive'); ?>
            <article id="post-<?php the_ID(); ?>" class="the-single single-artists-container col-lg-9 col-md-9 col-sm-9 col-xs-9 no-paddingl <?php echo join(' ', get_post_class()); ?>" itemscope itemtype="http://schema.org/Article">
                <header>
                    <h1 itemprop="name"><?php the_title(); ?></h1>
                </header>
                <?php if ( has_post_thumbnail()) : ?>
                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                    <?php the_post_thumbnail('full', $defaultargs); ?>
                </a>
                <?php endif; ?>
                <div class="post-content" itemprop="articleBody">
                    <?php the_content() ?>


                    <?php $files = rwmb_meta( 'rw_cat_file' ); ?>
                    <?php if ( !empty( $files ) ) { ?>
                    <?php foreach ( $files as $file ) { echo "<a href='{$file['url']}' title='{$file['title']}' target='_blank'><i class='fa fa-download fa-5x' aria-hidden='true'></i></a>"; } } ?>
                    <?php wp_link_pages( array(
    'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'g7galeria' ) . '</span>',
    'after'       => '</div>',
    'link_before' => '<span>',
    'link_after'  => '</span>', ) ); ?>
                </div><!-- .post-content -->
                <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                <meta itemprop="url" content="<?php the_permalink() ?>">
            </article> <?php // end article ?>
            <aside class="single-artists-aside col-lg-3 col-md-3 col-sm-3 col-xs-3 no-paddingr" role="complementary">
                <div id="sticker-artists">
                    <?php $post = get_post(get_post_meta(get_the_ID(),'rw_cat_artist', true)); ?>
                    <h3>Info Sobre: <br /><?php echo $post->post_title;  ?></h3>
                    <?php echo substr($post->post_content, 0, 190) . '...'; ?>
                    <a href="<?php echo get_permalink($post->ID); ?>" title="Ver más información"><button class="btn btn-md btn-custom">Ver Más Info.</button></a>
                </div>
            </aside>

        </div>
    </article>
</section>
<?php get_footer(); ?>
