<?php
/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_functions_overrides.php');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER CSS
-------------------------------------------------------------- */

function g7galeria_load_css() {
    if (!is_admin()){
        $version_remove = NULL;
        if ($_SERVER['REMOTE_ADDR'] == '::1') {

            /*- BOOTSTRAP CORE ON LOCAL -*/
            wp_register_style('bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', false, '3.3.7', 'all');
            wp_enqueue_style('bootstrap-css');

            /*- BOOTSTRAP THEME ON LOCAL -*/
            wp_register_style('bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css', array('bootstrap-css'), '3.3.7', 'all');
            wp_enqueue_style('bootstrap-theme');

            /*- ANIMATE CSS ON LOCAL -*/
            wp_register_style('animate-css', get_template_directory_uri() . '/css/animate.css', false, '3.5.1', 'all');
            wp_enqueue_style('animate-css');

            /*- FONT AWESOME ON LOCAL -*/
            wp_register_style('fontawesome-css', get_template_directory_uri() . '/css/font-awesome.min.css', false, '4.6.3', 'all');
            wp_enqueue_style('fontawesome-css');

            /*- FLICKITY ON LOCAL -*/
            //wp_register_style('flickity-css', get_template_directory_uri() . '/css/flickity.min.css', false, '2.0.2', 'all');
            //wp_enqueue_style('flickity-css');

            /*- OWL ON LOCAL -*/
            wp_register_style('owl-css', get_template_directory_uri() . '/css/owl.carousel.css', false, '1.3.3', 'all');
            wp_enqueue_style('owl-css');

            /*- OWL ON LOCAL -*/
            wp_register_style('owltheme-css', get_template_directory_uri() . '/css/owl.theme.css', false, '1.3.3', 'all');
            wp_enqueue_style('owltheme-css');

            /*- OWL ON LOCAL -*/
            wp_register_style('owltrans-css', get_template_directory_uri() . '/css/owl.transitions.css', false, '1.3.3', 'all');
            wp_enqueue_style('owltrans-css');

        } else {

            /*- BOOTSTRAP CORE -*/
            wp_register_style('bootstrap-css', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', false, '3.3.7', 'all');
            wp_enqueue_style('bootstrap-css');

            /*- BOOTSTRAP THEME -*/
            wp_register_style('bootstrap-theme', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css', array('bootstrap-css'), '3.3.7', 'all');
            wp_enqueue_style('bootstrap-theme');

            /*- ANIMATE CSS -*/
            wp_register_style('animate-css', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.1/animate.min.css', false, '3.5.1', 'all');
            wp_enqueue_style('animate-css');

            /*- FONT AWESOME -*/
            wp_register_style('fontawesome-css', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css', false, '4.6.3', 'all');
            wp_enqueue_style('fontawesome-css');

            /*- FLICKITY -*/
            //wp_register_style('flickity-css', 'https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.min.css', false, '2.0.2', 'all');
            //wp_enqueue_style('flickity-css');

            /*- OWL -*/
            wp_register_style('owl-css', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.css', false, '1.3.3', 'all');
            wp_enqueue_style('owl-css');

            /*- OWL -*/
            wp_register_style('owltheme-css', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.theme.min.css', false, '1.3.3', 'all');
            wp_enqueue_style('owltheme-css');

            /*- OWL -*/
            wp_register_style('owltrans-css', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.transitions.min.css', false, '1.3.3', 'all');
            wp_enqueue_style('owltrans-css');
        }

        /*- GOOGLE FONTS -*/
        wp_register_style('google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300,400,400italic,600,700,800,800italic', false, $version_remove, 'all');
        wp_enqueue_style('google-fonts');

        /*- MAIN STYLE -*/
        wp_register_style('main-style', get_template_directory_uri() . '/css/g7galeria-style.css', false, $version_remove, 'all');
        wp_enqueue_style('main-style');

        /*- MAIN MEDIAQUERIES -*/
        wp_register_style('main-mediaqueries', get_template_directory_uri() . '/css/g7galeria-mediaqueries.css', array('main-style'), $version_remove, 'all');
        wp_enqueue_style('main-mediaqueries');

        /*- WORDPRESS STYLE -*/
        wp_register_style('wp-initial-style', get_template_directory_uri() . '/style.css', false, $version_remove, 'all');
        wp_enqueue_style('wp-initial-style');
    }
}

add_action('init', 'g7galeria_load_css');

/* --------------------------------------------------------------
    ENQUEUE AND REGISTER JS
-------------------------------------------------------------- */

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", '');
function my_jquery_enqueue() {
    wp_deregister_script('jquery');
    if ($_SERVER['REMOTE_ADDR'] == '::1') {
        /*- JQUERY ON LOCAL  -*/
        wp_register_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, '1.12.4', false);
    } else {
        /*- JQUERY ON WEB  -*/
        wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js', false, '1.12.4', false);
    }
    wp_enqueue_script('jquery');
}


function g7galeria_load_js() {
    if (!is_admin()){
        $version_remove = NULL;
        if ($_SERVER['REMOTE_ADDR'] == '::1') {
            /*- MODERNIZR ON LOCAL  -*/
            wp_register_script('modernizr', get_template_directory_uri() . '/js/modernizr.min.js', array('jquery'), '2.8.3', true);
            wp_enqueue_script('modernizr');

            /*- BOOTSTRAP ON LOCAL  -*/
            wp_register_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.3.7', true);
            wp_enqueue_script('bootstrap');

            /*- JQUERY STICKY ON LOCAL  -*/
            wp_register_script('sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array('jquery'), '1.0.4', true);
            wp_enqueue_script('sticky');

            /*- JQUERY NICESCROLL ON LOCAL  -*/
            wp_register_script('nicescroll', get_template_directory_uri() . '/js/jquery.nicescroll.min.js', array('jquery'), '3.6.8', true);
            wp_enqueue_script('nicescroll');

            /*- LETTERING  -*/
            //wp_register_script('lettering', get_template_directory_uri() . '/js/jquery.lettering.js', array('jquery'), '0.7.0', true);
            //wp_enqueue_script('lettering');

            /*- SMOOTH SCROLL -*/
            //wp_register_script('smooth-scroll', get_template_directory_uri() . '/js/smooth-scroll.js', array('jquery'), '10.0.0', true);
            //wp_enqueue_script('smooth-scroll');

            /*- IMAGESLOADED ON LOCAL  -*/
            //wp_register_script('imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('imagesloaded');

            /*- ISOTOPE ON LOCAL  -*/
            //wp_register_script('isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array('jquery'), '3.0.1', true);
            //wp_enqueue_script('isotope');

            /*- FLICKITY ON LOCAL  -*/
            //wp_register_script('flickity', get_template_directory_uri() . '/js/flickity.pkgd.min.js', array('jquery'), '2.0.2', true);
            //wp_enqueue_script('flickity');

            /*- MASONRY ON LOCAL  -*/
            //wp_register_script('masonry', get_template_directory_uri() . '/js/masonry.pkgd.min.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('masonry');

            /*- OWL ON LOCAL -*/
            wp_register_script('owl-js', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.3.3', true);
            wp_enqueue_script('owl-js');

        } else {


            /*- MODERNIZR -*/
            wp_register_script('modernizr', 'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array('jquery'), '2.8.3', true);
            wp_enqueue_script('modernizr');

            /*- BOOTSTRAP -*/
            wp_register_script('bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), '3.3.7', true);
            wp_enqueue_script('bootstrap');

            /*- JQUERY STICKY -*/
            wp_register_script('sticky', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.sticky/1.0.3/jquery.sticky.min.js', array('jquery'), '1.0.3', true);
            wp_enqueue_script('sticky');

            /*- JQUERY NICESCROLL -*/
            wp_register_script('nicescroll', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js', array('jquery'), '3.6.8', true);
            wp_enqueue_script('nicescroll');

            /*- LETTERING  -*/
            //wp_register_script('lettering', 'https://cdnjs.cloudflare.com/ajax/libs/lettering.js/0.7.0/jquery.lettering.min.js', array('jquery'), '0.7.0', true);
            //wp_enqueue_script('lettering');

            /*- SMOOTH SCROLL -*/
            //wp_register_script('smooth-scroll', 'https://cdnjs.cloudflare.com/ajax/libs/smooth-scroll/10.0.0/js/smooth-scroll.min.js', array('jquery'), '10.0.0', true);
            //wp_enqueue_script('smooth-scroll');

            /*- IMAGESLOADED -*/
            //wp_register_script('imagesloaded', 'https://cdn.jsdelivr.net/imagesloaded/4.1.0/imagesloaded.pkgd.min.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('imagesloaded');

            /*- ISOTOPE -*/
            //wp_register_script('isotope', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.1/isotope.pkgd.min.js', array('jquery'), '3.0.1', true);
            //wp_enqueue_script('isotope');

            /*- FLICKITY -*/
            //wp_register_script('flickity', 'https://cdnjs.cloudflare.com/ajax/libs/flickity/2.0.2/flickity.pkgd.min.js', array('jquery'), '1.2.1', true);
            //wp_enqueue_script('flickity');

            /*- MASONRY -*/
            //wp_register_script('masonry', 'https://cdnjs.cloudflare.com/ajax/libs/masonry/4.1.0/masonry.pkgd.min.js', array('jquery'), '4.1.0', true);
            //wp_enqueue_script('masonry');

            /*- OWL ON LOCAL -*/
            wp_register_script('owl-js', 'https://cdnjs.cloudflare.com/ajax/libs/owl-carousel/1.3.3/owl.carousel.min.js', array('jquery'), '1.3.3', true);
            wp_enqueue_script('owl-js');

        }

        /*- MAIN FUNCTIONS -*/
        wp_register_script('main-functions', get_template_directory_uri() . '/js/functions.js', array('jquery'), $version_remove, true);
        wp_enqueue_script('main-functions');
    }
}

add_action('init', 'g7galeria_load_js');

/* --------------------------------------------------------------
    ADD THEME SUPPORT
-------------------------------------------------------------- */

load_theme_textdomain( 'g7galeria', get_template_directory() . '/languages' );
add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio' ));
add_theme_support( 'post-thumbnails' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );
add_theme_support( 'menus' );
add_theme_support( 'html5', array( 'comment-list', 'search-form', 'comment-form' ) );
add_theme_support( 'custom-background',
                  array(
    'default-image' => '',    // background image default
    'default-color' => '',    // background color default (dont add the #)
    'wp-head-callback' => '_custom_background_cb',
    'admin-head-callback' => '',
    'admin-preview-callback' => ''
)
                 );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */
function g7galeria_add_editor_styles() {
    add_editor_style( get_stylesheet_directory_uri() . '/css/editor-styles.css' );
}
add_action( 'admin_init', 'g7galeria_add_editor_styles' );

/* --------------------------------------------------------------
    ADD NAV MENUS LOCATIONS
-------------------------------------------------------------- */

register_nav_menus( array(
    'header_menu' => __( 'Menu Header Principal', 'g7galeria' ),
    'footer_menu' => __( 'Menu Footer', 'g7galeria' ),
) );

/* --------------------------------------------------------------
    ADD DYNAMIC SIDEBAR SUPPORT
-------------------------------------------------------------- */

add_action( 'widgets_init', 'g7galeria_widgets_init' );
function g7galeria_widgets_init() {
    register_sidebar( array(
        'name' => __( 'Main Sidebar', 'g7galeria' ),
        'id' => 'main_sidebar',
        'description' => __( 'Widgets seran vistos en posts y pages', 'g7galeria' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );

    register_sidebar( array(
        'name' => __( 'Shop Sidebar', 'g7galeria' ),
        'id' => 'shop_sidebar',
        'description' => __( 'Widgets seran vistos en Tienda y Categorias de Producto', 'g7galeria' ),
        'before_widget' => '<li id="%1$s" class="widget %2$s">',
        'after_widget'  => '</li>',
        'before_title'  => '<h2 class="widgettitle">',
        'after_title'   => '</h2>',
    ) );
}

/* --------------------------------------------------------------
    CUSTOM ADMIN LOGIN
-------------------------------------------------------------- */

add_action('login_head', 'custom_login_logo');
function custom_login_logo() {
    echo '
    <style type="text/css">
        body{
            background-color: #f5f5f5 !important;
            background-image:url(' . get_template_directory_uri() . '/images/bg.jpg);
            background-repeat: no-repeat;
            background-position: center;
            background-size: cover;
        }
        h1 {
            background-image:url(' . get_template_directory_uri() . '/images/logo.png) !important;
            background-repeat: no-repeat;
            background-position: center;
            background-size: contain !important;
        }
        a { background-image:none !important; }
        .login form{
            -webkit-border-radius: 5px;
            border-radius: 5px;
            background-color: rgba(203, 203, 203, 0.5);
            box-shadow: 0px 0px 6px #878787;
        }
        .login label{
            color: black;
            font-weight: 500;
        }
    </style>
    ';
}

if (! function_exists('dashboard_footer') ){
    function dashboard_footer() {
        echo '<span id="footer-thankyou">';
        _e ('Gracias por crear con ', 'g7galeria' );
        echo '<a href="http://wordpress.org/" >WordPress.</a> - ';
        _e ('Tema desarrollado por ', 'g7galeria' );
        echo '<a href="http://robertochoa.com.ve/" >Robert Ochoa</a></span>';
    }
}
add_filter('admin_footer_text', 'dashboard_footer');

/* --------------------------------------------------------------
    PRODUCT LIST
-------------------------------------------------------------- */

function prodList() {
    global $wpdb;
    $the_query = "SELECT ID, post_title FROM " . $wpdb->prefix . "posts WHERE post_type LIKE 'artistas' AND post_status LIKE 'publish' ORDER BY post_date ASC";
    $posts_year_range = $wpdb->get_results($the_query, 'ARRAY_A');
    $item_container = [];
    $i = 1;

    foreach ($posts_year_range as $item){
        $itemkeys[] = $item['ID'];
        $itemvalues[] = $item['post_title'];
    }
    $item_container[] = array_combine($itemkeys, $itemvalues);

    return $item_container[0];
}

/* --------------------------------------------------------------
    ADD CUSTOM METABOX
-------------------------------------------------------------- */

add_filter( 'rwmb_meta_boxes', 'g7galeria_metabox' );

function g7galeria_metabox( $meta_boxes )
{
    $prefix = 'rw_';

    $meta_boxes[] = array(
        'title'    => __('Información del Catálogo:', 'g7galeria' ),
        'pages'    => array( 'catalogo' ),
        'fields' => array(
        array(
        'name'  => __( 'Artista:', 'g7galeria' ),
        'desc'  => 'Artista del Catálogo',
        'id'    => $prefix . 'cat_artist',
        'type' => 'select_advanced',
        'multiple' => true,
        'options' => prodList(),
    ),
        array(
        'name'  => __( 'Fecha:', 'g7galeria' ),
        'desc'  => 'Fecha del Catálogo',
        'id'    => $prefix . 'date_artist',
        'type'  => 'date',
        'js_options' => array(
        'dateFormat'      => esc_html__( 'dd-mm-yy', 'g7galeria' ),
        'changeMonth'     => true,
        'changeYear'      => true,
        'showButtonPanel' => true,
    ),
    ),
        array(
        'name'             => __( 'Archivo', 'g7galeria' ),
        'id'               => $prefix . 'cat_file',
        'type'             => 'file',
        // Delete file from Media Library when remove it from post meta?
        // Note: it might affect other posts if you use same file for multiple posts
        'force_delete'     => false,
        // Maximum file uploads
        'max_file_uploads' => 1,
    ),
    ),
    );

    $meta_boxes[] = array(
        'title'    => __('Información de Exposición:', 'g7galeria' ),
        'pages'    => array( 'exposiciones' ),
        'context'    => 'side',
        'priority'   => 'high',
        'fields' => array(
        array(
        'name'  => __( 'Artista:', 'g7galeria' ),
        'desc'  => 'Artista de la Exposición',
        'id'    => $prefix . 'expo_artist',
        'type' => 'select_advanced',
        'multiple' => true,
        'options' => prodList(),
    ),
        array(
        'name'  => __( 'Fecha:', 'g7galeria' ),
        'desc'  => 'Fecha de la Exposición',
        'id'    => $prefix . 'date_expo',
        'type'  => 'date',
        'js_options' => array(
        'dateFormat'      => esc_html__( 'dd-mm-yy', 'g7galeria' ),
        'changeMonth'     => true,
        'changeYear'      => true,
        'showButtonPanel' => true,
    ),
    ),
    ),
    );

    return $meta_boxes;
}

/* --------------------------------------------------------------
    ADD CUSTOM POST TYPE
-------------------------------------------------------------- */

// Register Custom Post Type
function slider() {

    $labels = array(
        'name'                  => _x( 'Slides', 'Post Type General Name', 'g7galeria' ),
        'singular_name'         => _x( 'Slide', 'Post Type Singular Name', 'g7galeria' ),
        'menu_name'             => __( 'Slider', 'g7galeria' ),
        'name_admin_bar'        => __( 'Slider', 'g7galeria' ),
        'archives'              => __( 'Archivo de Slides', 'g7galeria' ),
        'parent_item_colon'     => __( 'Slide Padre:', 'g7galeria' ),
        'all_items'             => __( 'Todos los Slides', 'g7galeria' ),
        'add_new_item'          => __( 'Agregar Nuevo Slide', 'g7galeria' ),
        'add_new'               => __( 'Agregar Nuevo', 'g7galeria' ),
        'new_item'              => __( 'Nuevo Slide', 'g7galeria' ),
        'edit_item'             => __( 'Editar Slide', 'g7galeria' ),
        'update_item'           => __( 'Actualizar Slide', 'g7galeria' ),
        'view_item'             => __( 'Ver Slide', 'g7galeria' ),
        'search_items'          => __( 'Buscar Slide', 'g7galeria' ),
        'not_found'             => __( 'No hay resultados', 'g7galeria' ),
        'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'g7galeria' ),
        'featured_image'        => __( 'Imagen de Slide', 'g7galeria' ),
        'set_featured_image'    => __( 'Colocar Imagen de Slide', 'g7galeria' ),
        'remove_featured_image' => __( 'Remover Imagen de Slide', 'g7galeria' ),
        'use_featured_image'    => __( 'Usar como Imagen de Slide', 'g7galeria' ),
        'insert_into_item'      => __( 'Insertar en Slide', 'g7galeria' ),
        'uploaded_to_this_item' => __( 'Cargado a este Slide', 'g7galeria' ),
        'items_list'            => __( 'Listado de Slides', 'g7galeria' ),
        'items_list_navigation' => __( 'Navegación del Listado de Slides', 'g7galeria' ),
        'filter_items_list'     => __( 'Filtro del Listado de Slides', 'g7galeria' ),
    );
    $args = array(
        'label'                 => __( 'Slide', 'g7galeria' ),
        'description'           => __( 'Slider en homepage', 'g7galeria' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'thumbnail', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 30,
        'menu_icon'             => 'dashicons-slides',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
    );
    register_post_type( 'slider', $args );

}
add_action( 'init', 'slider', 0 );

function exposiciones() {

    $labels = array(
        'name'                  => _x( 'Exposiciones', 'Post Type General Name', 'g7galeria' ),
        'singular_name'         => _x( 'Exposición', 'Post Type Singular Name', 'g7galeria' ),
        'menu_name'             => __( 'Exposiciones', 'g7galeria' ),
        'name_admin_bar'        => __( 'Exposiciones', 'g7galeria' ),
        'archives'              => __( 'Archivo de Exposiciones', 'g7galeria' ),
        'parent_item_colon'     => __( 'Exposición Padre:', 'g7galeria' ),
        'all_items'             => __( 'Todas las Exposiciones', 'g7galeria' ),
        'add_new_item'          => __( 'Agregar Nueva Exposición', 'g7galeria' ),
        'add_new'               => __( 'Agregar Nuevo', 'g7galeria' ),
        'new_item'              => __( 'Nueva Exposición', 'g7galeria' ),
        'edit_item'             => __( 'Editar Exposición', 'g7galeria' ),
        'update_item'           => __( 'Actualizar Exposición', 'g7galeria' ),
        'view_item'             => __( 'Ver Exposición', 'g7galeria' ),
        'search_items'          => __( 'Buscar Exposición', 'g7galeria' ),
        'not_found'             => __( 'No hay resultados', 'g7galeria' ),
        'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'g7galeria' ),
        'featured_image'        => __( 'Imagen de Portada', 'g7galeria' ),
        'set_featured_image'    => __( 'Colocar Imagen de Portada', 'g7galeria' ),
        'remove_featured_image' => __( 'Remover Imagen de Portada', 'g7galeria' ),
        'use_featured_image'    => __( 'Usar como Imagen de Portada', 'g7galeria' ),
        'insert_into_item'      => __( 'Insertar en Exposición', 'g7galeria' ),
        'uploaded_to_this_item' => __( 'Cargado a esta Exposición', 'g7galeria' ),
        'items_list'            => __( 'Listado de Exposiciones', 'g7galeria' ),
        'items_list_navigation' => __( 'Navegación del Listado de Exposiciones', 'g7galeria' ),
        'filter_items_list'     => __( 'Filtro del Listado de Exposiciones', 'g7galeria' ),
    );
    $args = array(
        'label'                 => __( 'Exposición', 'g7galeria' ),
        'description'           => __( 'Exposiciones dentro de la Galería', 'g7galeria' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'taxonomies'            => array( 'custom_expo' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-screenoptions',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'exposiciones', $args );

}
add_action( 'init', 'exposiciones', 0 );

// Register Custom Post Type
function artistas() {

    $labels = array(
        'name'                  => _x( 'Artistas', 'Post Type General Name', 'g7galeria' ),
        'singular_name'         => _x( 'Artista', 'Post Type Singular Name', 'g7galeria' ),
        'menu_name'             => __( 'Artistas', 'g7galeria' ),
        'name_admin_bar'        => __( 'Artistas', 'g7galeria' ),
        'archives'              => __( 'Archivo de Artistas', 'g7galeria' ),
        'parent_item_colon'     => __( 'Artista Padre:', 'g7galeria' ),
        'all_items'             => __( 'Todos los Artistas', 'g7galeria' ),
        'add_new_item'          => __( 'Agregar Nuevo Artista', 'g7galeria' ),
        'add_new'               => __( 'Agregar Nuevo', 'g7galeria' ),
        'new_item'              => __( 'Nuevo Artista', 'g7galeria' ),
        'edit_item'             => __( 'Editar Artista', 'g7galeria' ),
        'update_item'           => __( 'Actualizar Artista', 'g7galeria' ),
        'view_item'             => __( 'Ver Artista', 'g7galeria' ),
        'search_items'          => __( 'Buscar Artista', 'g7galeria' ),
        'not_found'             => __( 'No hay resultados', 'g7galeria' ),
        'not_found_in_trash'    => __( 'No hay resultados en papelera', 'g7galeria' ),
        'featured_image'        => __( 'Imagen del Artista', 'g7galeria' ),
        'set_featured_image'    => __( 'Colocar Imagen del Artista', 'g7galeria' ),
        'remove_featured_image' => __( 'Remover Imagen del Artista', 'g7galeria' ),
        'use_featured_image'    => __( 'Usar como Imagen del Artista', 'g7galeria' ),
        'insert_into_item'      => __( 'Insertar en Artista', 'g7galeria' ),
        'uploaded_to_this_item' => __( 'Cargado a este Artista', 'g7galeria' ),
        'items_list'            => __( 'Listado de Artistas', 'g7galeria' ),
        'items_list_navigation' => __( 'Navegador del Listado de Artistas', 'g7galeria' ),
        'filter_items_list'     => __( 'Filtro del Listado de Artistas', 'g7galeria' ),
    );
    $args = array(
        'label'                 => __( 'Artista', 'g7galeria' ),
        'description'           => __( 'Artistas participantes en la Galería', 'g7galeria' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'taxonomies'            => array( 'custom_artistas' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-universal-access-alt',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'artistas', $args );

}
add_action( 'init', 'artistas', 0 );

// Register Custom Post Type
function catalogo() {

    $labels = array(
        'name'                  => _x( 'Catálogos', 'Post Type General Name', 'g7galeria' ),
        'singular_name'         => _x( 'Catálogo', 'Post Type Singular Name', 'g7galeria' ),
        'menu_name'             => __( 'Catálogos', 'g7galeria' ),
        'name_admin_bar'        => __( 'Catálogos', 'g7galeria' ),
        'archives'              => __( 'Archivo de Catálogos', 'g7galeria' ),
        'parent_item_colon'     => __( 'Catálogo Padre:', 'g7galeria' ),
        'all_items'             => __( 'Todos los Catálogos', 'g7galeria' ),
        'add_new_item'          => __( 'Agregar Nuevo Catálogo', 'g7galeria' ),
        'add_new'               => __( 'Agregar Nuevo', 'g7galeria' ),
        'new_item'              => __( 'Nuevo Catálogo', 'g7galeria' ),
        'edit_item'             => __( 'Editar Catálogo', 'g7galeria' ),
        'update_item'           => __( 'Actualizar Catálogo', 'g7galeria' ),
        'view_item'             => __( 'Ver Catálogo', 'g7galeria' ),
        'search_items'          => __( 'Buscar Catálogo', 'g7galeria' ),
        'not_found'             => __( 'No hay resultados', 'g7galeria' ),
        'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'g7galeria' ),
        'featured_image'        => __( 'Imagen descriptiva', 'g7galeria' ),
        'set_featured_image'    => __( 'Colocar Imagen descriptiva', 'g7galeria' ),
        'remove_featured_image' => __( 'Remover Imagen descriptiva', 'g7galeria' ),
        'use_featured_image'    => __( 'Usar como Imagen descriptiva', 'g7galeria' ),
        'insert_into_item'      => __( 'Insertar en Catálogo', 'g7galeria' ),
        'uploaded_to_this_item' => __( 'Cargado a este Catálogo', 'g7galeria' ),
        'items_list'            => __( 'Listado de Catálogos', 'g7galeria' ),
        'items_list_navigation' => __( 'Navegación del Listado de Catálogos', 'g7galeria' ),
        'filter_items_list'     => __( 'Filtro del Listado de Catálogos', 'g7galeria' ),
    );
    $args = array(
        'label'                 => __( 'Catálogo', 'g7galeria' ),
        'description'           => __( 'Catalogos de la Galería', 'g7galeria' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'taxonomies'            => array( 'custom_catalogo' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-clipboard',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'catalogo', $args );

}
add_action( 'init', 'catalogo', 0 );

/* --------------------------------------------------------------
    ADD CUSTOM IMAGE SIZE
-------------------------------------------------------------- */
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 9999, 400, true);
}
if ( function_exists( 'add_image_size' ) ) {
    add_image_size('avatar', 100, 100, true);
    add_image_size('slider_img', 9999, 350, array('center','center'));
    add_image_size('expo_img', 600, 400, array('center','center'));
    add_image_size('expo_archive_img', 250, 215, array('center','center'));
    add_image_size('artist_img', 270, 230, array('center','center'));
}


/* --------------------------------------------------------------
    ADD CUSTOM WALKER BOOTSTRAP
-------------------------------------------------------------- */

// WALKER COMPLETO TOMADO DESDE EL NAVBAR COLLAPSE
require_once('includes/wp_bootstrap_navwalker.php');

// WALKER CUSTOM SI DEBO COLOCAR ICONOS AL LADO DEL MENU PRINCIPAL - SU ESTRUCTURA ESTA DENTRO DEL MISMO ARCHIVO
//require_once('includes/wp_walker_custom.php');

/* --------------------------------------------------------------
    ADD CUSTOM WORDPRESS FUNCTIONS
-------------------------------------------------------------- */

require_once('includes/wp_custom_functions.php');

/* --------------------------------------------------------------
    ADD CUSTOM WOOCOMMERCE OVERRIDES
-------------------------------------------------------------- */

//require_once('includes/wp_woocommerce_functions.php');

add_action( 'pre_get_posts', 'my_change_sort_order'); 
function my_change_sort_order($query){
    if(is_post_type_archive('artistas')):
    //If you wanted it for the archive of a custom post type use: is_post_type_archive( $post_type )
    //Set the order ASC or DESC
    $query->set( 'order', 'ASC' );
    //Set the orderby
    $query->set( 'orderby', 'title' );
    endif;    
};

add_action('manage_exposiciones_posts_custom_column', 'ST4_columns_content', 10, 2);

function ST4_columns_content($column_name, $post_ID) {

}

// ONLY MOVIE CUSTOM TYPE POSTS
add_filter('manage_exposiciones_posts_columns', 'ST4_columns_head_only_movies', 10);
add_action('manage_exposiciones_posts_custom_column', 'ST4_columns_content_only_movies', 10, 2);

// CREATE TWO FUNCTIONS TO HANDLE THE COLUMN
function ST4_columns_head_only_movies($defaults) {
    $defaults['featured_image'] = 'Portada de Exposición';
    
    return $defaults;
}
function ST4_columns_content_only_movies($column_name, $post_ID) {
    if ($column_name == 'featured_image') {
        $post_featured_image = ST4_get_featured_image($post_ID);
        if ($post_featured_image) {
            // HAS A FEATURED IMAGE
            echo '<img src="' . $post_featured_image . '" style="width: 100px; height: auto;"/>';
        }
        else {
            // NO FEATURED IMAGE, SHOW THE DEFAULT ONE
            echo '<img src="' . get_bloginfo( 'template_url' ) . '/images/no-img.jpg"  style="width: 100px; height: auto;" />';
        }
    }
}

function ST4_get_featured_image($post_ID) {
    $post_thumbnail_id = get_post_thumbnail_id($post_ID);
    if ($post_thumbnail_id) {
        $post_thumbnail_img = wp_get_attachment_image_src($post_thumbnail_id, 'avatar');
        return $post_thumbnail_img[0];
    }
}

?>
