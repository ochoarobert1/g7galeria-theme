<?php get_header(); ?>
<?php the_post(); ?>
<section class="page-container col-lg-9 col-md-9 col-sm-9 col-xs-9" role="article" itemscope itemtype="http://schema.org/BlogPosting">
    <h1 itemprop="headline"><?php the_title(); ?></h1>
    <article id="post-<?php the_ID(); ?>" class="page-content <?php echo join(' ', get_post_class()); ?>" >
        <div class="page-article col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" itemprop="articleBody">
            <?php the_content(); ?>
        </div>

    </article>
</section>
<?php get_footer(); ?>


