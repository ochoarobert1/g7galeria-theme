</div>
</div>
<footer class="container-fluid"  role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row">
        <div class="the-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="footer-item col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <p><i class="fa fa-map-marker"></i>Octava Transversal</p>
                        <p>con Avenida Ávila. </p>
                        <p>Los Chorros Caracas 107</p>
                        <p>Martes - Sábados </p>
                        <p>11am - 6pm</p>
                        <p>Domingos</p>
                        <p>11am - 4pm</p>
                    </div>
                    <div class="footer-item col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <?php wp_nav_menu(array('container_class' => 'menu-footer', 'theme_location' => 'footer_menu' )); ?>
                        <div class="social-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                            <span>Síguenos</span>
                            <i class="fa fa-instagram"></i>
                            <i class="fa fa-twitter"></i>
                            <i class="fa fa-facebook"></i>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                            <p><a href="mailto:gsietetiendagaleria@gmail.com">gsietetiendagaleria@gmail.com</a></p>
                            <p>telf: 0212 286 87 31</p>
                        </div>
                    </div>
                    <div class="footer-item col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <?php get_template_part('templates/map'); ?>
                    </div>
                    <div class="footer-item col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-subscribe.png" alt="suscríbete" class="img-responsive" />
                        <h6>powered by <a href="http://corporaciond1.com" target="_blank" title="Corporación d1">Corporación d1</a></h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>

</body>
</html>
