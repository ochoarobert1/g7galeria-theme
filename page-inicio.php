<?php get_header(); ?>
<?php the_post(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<section class="page-container col-lg-9 col-md-9 col-sm-9 col-xs-9" role="article" itemscope itemtype="http://schema.org/BlogPosting">
    <article id="post-<?php the_ID(); ?>" class="page-content  col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr <?php echo join(' ', get_post_class()); ?>" >
        <div class="page-article col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" itemprop="articleBody">
            <?php $args = array('post_type' => 'slider', 'posts_per_page' => 4, 'order' => 'ASC', 'orderby' => 'date'); ?>
            <?php query_posts($args); ?>
            <?php if (have_posts()) : ?>
            <section class="home-slider-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                <?php while (have_posts()) : the_post(); ?>
                <div class="home-slider-item col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <?php the_post_thumbnail('full', $defaultatts); ?>
                </div>
                <?php endwhile; ?>
            </section>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            <?php $args = array('post_type' => 'exposiciones', 'posts_per_page' => 4, 'order' => 'DESC', 'orderby' => 'date'); ?>
            <?php query_posts($args); ?>
            <?php if (have_posts()) : ?>
            <article class="home-expo-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                <h2>EXPOSICIONES</h2>
                <?php while (have_posts()) : the_post(); ?>
                <div class="home-expo-item col-lg-6 col-md-6 col-sm-6 col-xs-6">
                    <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                        <?php the_post_thumbnail('expo_img', $defaultatts); ?>
                    </a>
                    <h3><?php the_title(); ?></h3>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7 no-paddingl">
                        <?php $artist_id = get_post_meta(get_the_ID(), 'rw_expo_artist', true); ?>
                        <?php $post_artist = get_post($artist_id); ?>
                        <p><?php echo $post_artist->post_title; ?></p>    
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right">
                        <p><?php echo get_post_meta(get_the_ID(), 'rw_date_expo', true); ?></p>    
                    </div>

                </div>
                <?php endwhile; ?>
            </article>
            <?php endif; ?>
            <?php wp_reset_query(); ?>
            <?php the_content(); ?>
        </div>
    </article>
</section>
<?php get_footer(); ?>
