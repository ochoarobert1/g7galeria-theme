<?php get_header(); ?>
<section class="page-container col-lg-9 col-md-9 col-sm-9 col-xs-9" role="article" itemscope itemtype="http://schema.org/BlogPosting">
    <article id="post-<?php the_ID(); ?>" class="page-content <?php echo join(' ', get_post_class()); ?>" >
        <div class="page-article col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" itemprop="articleBody">
            <h1><?php echo get_the_archive_title(); ?></h1>
            <hr>
            <div class="archive-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                <?php $defaultatts = array('class' => 'img-responsive'); ?>
                <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" class="archive-item col-lg-4 col-md-4 col-sm-4 col-xs-4 <?php echo join(' ', get_post_class()); ?>" role="article">
                    <picture class="archive-item-picture-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                        <?php if ( has_post_thumbnail()) : ?>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            <?php the_post_thumbnail('artist_img', $defaultatts); ?>
                        </a>
                        <?php else : ?>
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/no-img.jpg" alt="No img" class="img-responsive" />
                        </a>
                        <?php endif; ?>
                        <div class="archive-item-title-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><h2 itemprop="title" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2></a>
                        </div>
                    </picture>
                </article>
                <?php endwhile; ?>
                <div class="pagination col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <?php if(function_exists('wp_paginate')) { wp_paginate(); } else { posts_nav_link(); wp_link_pages(); } ?>
                </div>
            </div>
            <?php else: ?>
            <article>
                <h2>Disculpe, su busqueda no arrojo ningun resultado</h2>
                <h3>Haga click <a href="<?php echo home_url('/'); ?>">aqui</a> para volver al inicio</h3>
            </article>
            <?php endif; ?>
        </div>
    </article>
</section>
<?php get_footer(); ?>
