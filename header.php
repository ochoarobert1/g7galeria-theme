<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <?php /* MAIN STUFF */ ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset') ?>" />
        <meta name="robots" content="NOODP, INDEX, FOLLOW" />
        <meta name="HandheldFriendly" content="True" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="dns-prefetch" href="//connect.facebook.net" />
        <link rel="dns-prefetch" href="//facebook.com" />
        <link rel="dns-prefetch" href="//googleads.g.doubleclick.net" />
        <link rel="dns-prefetch" href="//pagead2.googlesyndication.com" />
        <link rel="dns-prefetch" href="//google-analytics.com" />
        <?php /* FAVICONS */ ?>
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />
        <?php /* THEME NAVBAR COLOR */ ?>
        <meta name="msapplication-TileColor" content="#BD004C" />
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/win8-tile-icon.png" />
        <meta name="theme-color" content="#BD004C" />
        <?php /* AUTHOR INFORMATION */ ?>
        <meta name="language" content="<?php echo get_bloginfo('language'); ?>" />
        <meta name="author" content="gsietegaleria" />
        <meta name="copyright" content="http://gsietegaleria.com" />
        <meta name="geo.position" content="10.5034724,-66.8336642" />
        <meta name="ICBM" content="10.5034724,-66.8336642" />
        <meta name="geo.region" content="VE" />
        <meta name="geo.placename" content="Centro de Arte Los Galpones. Calle Ávila con 8va Transversal, Los Chorros Caracas" />
        <meta name="DC.title" content="<?php if (is_home()) { echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); } else { echo get_the_title() . ' | ' . get_bloginfo('name'); } ?>" />
        <?php /* MAIN TITLE - CALL HEADER MAIN FUNCTIONS */ ?>
        <?php wp_title('|', false, 'right'); ?>
        <?php wp_head() ?>
        <?php /* OPEN GRAPHS INFO - COMMENTS SCRIPTS */ ?>
        <?php get_template_part('includes/header-oginfo'); ?>
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php /* IE COMPATIBILITIES */ ?>
        <!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7" /><![endif]-->
        <!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8" /><![endif]-->
        <!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9" /><![endif]-->
        <!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js" /><!--<![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script> <![endif]-->
        <!--[if IE]> <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" /> <![endif]-->
        <?php get_template_part('includes/fb-script'); ?>
        <?php get_template_part('includes/ga-script'); ?>
    </head>
    <body class="the-main-body <?php echo join(' ', get_body_class()); ?>" itemscope itemtype="http://schema.org/WebPage">

        <div id="fb-root"></div>
        <?php if (is_front_page()) { ?>
        <div class="preloader">
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
        </div>
        <?php } ?>
        <header class="container" role="banner" itemscope itemtype="http://schema.org/WPHeader">
            <div class="row">
                <div class="the-header col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <a href="<?php echo home_url('/'); ?>" title="Inicio">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="Gsietegalería" class="img-responsive img-logo"/>
                        </a>
                    </div>
                    <div class="lang-selector col-md-3 col-md-offset-6 col-sm-3 col-sm-offset-6 col-xs-9">
                        <span><a href="">Español</a> | <a href="">English</a></span>
                        <div class="social-header  col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <a href=""><i class="fa fa-facebook"></i></a>
                            <a href=""><i class="fa fa-twitter"></i></a>
                            <a href=""><i class="fa fa-instagram"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div class="container">
            <div class="row">
                <nav class="the-navbar col-lg-3 col-md-3 col-sm-3 col-xs-3">
                    <div id="sticker">
                        <?php wp_nav_menu(array('theme_location' => 'header_menu')); ?>
                    </div>
                </nav>




